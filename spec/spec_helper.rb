require "yaml"
require "active_record"
require_relative "../lib/configuration"

ENV["RACK_ENV"] ||= "test"
ActiveRecord::Base.establish_connection(YAML.load(File.read("config.yml"))[ENV["RACK_ENV"]]["database"])
