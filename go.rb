require "yaml"
require "active_record"
require_relative "./lib/configuration"

ActiveRecord::Base.establish_connection(YAML.load(File.read("config.yml"))[ENV['RACK_ENV']]["database"])

puts "Strings..."
Configuration["string"] = "hello!"
puts Configuration["string"]

puts "Integers..."
Configuration["int"] = 3
puts Configuration["int"]

puts "Floats..."
Configuration["float"] = 2.718
puts Configuration["float"]

puts "Booleans..."
Configuration["bool"] = true
puts Configuration["bool"]

puts "All..."
Configuration.all.each do |k,v|
  puts "#{k} => #{v}"
end
