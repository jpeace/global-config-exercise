require "rspec/core/rake_task"
require "yaml"
require "active_record"
require "mysql2"

environments = ["development", "test"]
ENV["RACK_ENV"] ||= "development"
raise "Unknown Rack environment" unless environments.include?(ENV["RACK_ENV"])

config = YAML.load(File.read("config.yml"))
db_config = config[ENV['RACK_ENV']]["database"]

task :default => ["spec"]

desc "Run specs"
RSpec::Core::RakeTask.new(:spec => :"db:migrate") do |t|
  t.rspec_opts = "-cfd --require spec_helper"
end

namespace :db do
  desc "Creates the databases for each environment"
  task :create do
    environments.each do |env|
      client = Mysql2::Client.new(
        host: config["env"]["database"]["host"],
        username: config["env"]["database"]["username"],
        password: config["env"]["database"]["password"])
      client.query("CREATE DATABASE #{ config["env"]["database"]["database"] }")
    end
  end

  desc "Migrates the database for the current Rack environment, use $VERSION to pick migration version"
  task :migrate => :"db:environment" do
    ActiveRecord::Migrator.migrate("db/migrate", ENV["VERSION"] ? ENV["VERSION"].to_i : nil )
  end

  desc "Initialize ActiveRecord environment"
  task :environment do
    ActiveRecord::Base.establish_connection(db_config)
  end
end
