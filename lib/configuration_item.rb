require "active_record"

class ConfigurationItem < ActiveRecord::Base
  validates :key, :value, :data_type, presence: true
  validates :data_type, inclusion: { in: %w(string integer float boolean) }

  # Returns this item's value as the appropriate type
  # Returns:
  #   Value converted to the appropriate type
  def typed_value
    return nil if value.nil?

    case data_type
    when "string"
      value.to_s
    when "integer"
      value.to_i
    when "float"
      value.to_f
    when "boolean"
      value == "true"
    else
      raise "Unknown value type"
    end
  end

  # Determines whether this item is empty i.e. if any of its attributes are missing
  # Returns:
  #   True if the item is empty, false otherwise
  def empty?
    key.nil? || value.nil? || data_type.nil?
  end

  # Constructs an empty ConfigurationItem
  # Returns
  #   An empty ConfigurationItem
  def self.empty
    ConfigurationItem.new(key: nil, value: nil, data_type: nil)
  end
end
