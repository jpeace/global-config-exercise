describe Configuration do
  context "determining type from value" do
    it "works with strings" do
      expect(described_class.type_of_value("test")).to eq "string"
    end

    it "works with integers" do
      expect(described_class.type_of_value(5)).to eq "integer"
    end

    it "works with floats" do
      expect(described_class.type_of_value(3.14)).to eq "float"
    end

    it "works with booleans" do
      expect(described_class.type_of_value(false)).to eq "boolean"
    end

    it "returns nil on unknown types" do
      expect(described_class.type_of_value({a: 12, b: 34})).to be_nil
    end
  end

  context "reading and writing values" do
    before(:each) do
      described_class.clear!
    end

    it "correctly reads and writes strings" do
      described_class["mykey"] = "test"

      expect(described_class["mykey"]).to eq "test"
      expect(described_class["mykey"].class).to eq String
    end

    it "correctly reads and writes integers" do
      described_class["mykey"] = 5

      expect(described_class["mykey"]).to eq 5
      expect(described_class["mykey"].class).to eq Fixnum
    end

    it "correctly reads and writes floats" do
      described_class["mykey"] = 3.14

      expect(described_class["mykey"]).to eq 3.14
      expect(described_class["mykey"].class).to eq Float
    end

    it "correctly reads and writes strings" do
      described_class["mykey"] = false

      expect(described_class["mykey"]).to eq false
      expect(described_class["mykey"].class).to eq FalseClass
    end

    it "overwrites existing values" do
      described_class["mykey"] = "hello"
      expect(described_class.all.length).to eq 1
      expect(described_class["mykey"]).to eq "hello"
      
      described_class["mykey"] = "world"
      expect(described_class.all.length).to eq 1
      expect(described_class["mykey"]).to eq "world"
    end

    it "reads empty values as nil" do
      expect(described_class["empty"]).to be_nil
    end

    it "retrieves all configuration values" do
      described_class["key1"] = "hello!"
      described_class["key2"] = 17
      described_class["key3"] = true

      all = described_class.all

      expect(all.length).to eq 3
      expect(all["key1"]).to eq "hello!"
      expect(all["key2"]).to eq 17
      expect(all["key3"]).to eq true
    end
  end
end
