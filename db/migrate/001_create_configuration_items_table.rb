class CreateConfigurationItemsTable < ActiveRecord::Migration
  def self.up
    create_table :configuration_items do |t|
      t.string :key, null: false
      t.string :value, null: false
      t.string :data_type, null: false
    end

    add_index :configuration_items, :key
  end

  def self.down
    drop_table :configuration_items
  end
end
