require_relative "configuration_item"

class Configuration
  class << self

    # Reads a value from global configuration
    # Params:
    #   key - The configuration key for lookup
    # Returns:
    #   The typed configuration value, or nil if the value for the given key does not exist
    def [](key)
      raise "Key must be a string" unless key.is_a? String
      read(key)
    end

    # Writes a value to global configuration
    # Params:
    #   key - The configuration key to store this value under
    #   val - The value to store in global configuration
    def []=(key, val)
      raise "Key must be a string" unless key.is_a? String
      raise "No value given" if val.nil?

      type = type_of_value(val)
      raise "Invalid type: #{val.class}" if type.nil?

      write(key, val, type)
    end

    # Clears the global configuration
    def clear!
      ConfigurationItem.delete_all
    end

    # Retrieves the global configuration
    # Returns:
    #   A hash containing the entire global configuration
    def all
      Hash[ConfigurationItem.all.map { |i| [i.key, i.typed_value] }]
    end

    # Gives a string representation of a value's class for supported data types
    # Params:
    #   val - The value to resolve data type for
    # Returns:
    #   A string representation of val's type
    def type_of_value(val)
      # Using stringified class name since case equality for Class apparently does not behave as you'd expect
      # e.g. String === String => false
      case val.class.to_s
      when "String"
        "string"
      when "Fixnum"
        "integer"
      when "Float"
        "float"
      when "TrueClass", "FalseClass"
        "boolean"
      else
        nil
      end
    end

    private

    def cache
      @cache ||= ActiveSupport::Cache::MemoryStore.new
    end

    # Updates the cache for the given configuration item
    # Params:
    #   item - The ConfigurationItem to cache. Will skip if the item is empty
    # Returns:
    #   The given ConfigurationItem
    def update_cache(item)
      cache.write(item.key, item.typed_value) unless item.empty?
      item
    end

    # Looks up a ConfigurationItem in the database
    # Params:
    #   key - The key to use for lookup
    # Returns:
    #   The ConfigurationItem if it exists, or an empty ConfigurationItem otherwise
    def find(key)
      (ConfigurationItem.find_by_key(key) || ConfigurationItem.empty)
    end

    # Reads a configuration value from cache or from the database
    # Params:
    #   key - The key to use for lookup
    # Returns:
    #   The typed configuration value for key if it exists, nil otherwise
    def read(key)
      cache.read(key) || update_cache(find(key)).typed_value
    end

    # Writes a configuration value to the database
    # Params:
    #   key - The key to store this value under
    #   val - The value to store
    #   type - The type of val
    def write(key, val, type)
      item = find(key)
      item.attributes = {key: key, value: val.to_s, data_type: type}
      if item.save
        update_cache(item)
      else
        raise "Could not write value: #{item.errors.messages}"
      end
      
      item.typed_value
    end
  end
end
